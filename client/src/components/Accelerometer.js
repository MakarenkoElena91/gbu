import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
} from 'react-native';
import {
  accelerometer,
  setUpdateIntervalForType,
  SensorTypes,
} from 'react-native-sensors';

setUpdateIntervalForType(SensorTypes.accelerometer, 400);

export default class Accelerometer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      x: 0,
      y: 0,
      z: 0,
    };
  }

  componentDidMount() {
    const accelerometerSubscription = accelerometer.subscribe(({x, y, z}) => {
      this.setState(state => ({
        x: x + state.x,
        y: y + state.y,
        z: z + state.z,
      }));
    });

    this.setState({accelerometerSubscription});
  }

  componentWillUnmount() {
    this.state.accelerometerSubscription.unsubscribe();
  }

  render() {
    const x = this.state.x;
    const y = this.state.y;
    const z = this.state.z;

    return (
      <View style={styles.container}>
        <View>
          <Text>
            Accelerometer values
          </Text>
          <Text>
            x={x.toFixed(2)}
          </Text>
          <Text>
            y={y.toFixed(2)}
          </Text>
          <Text>
            z={z.toFixed(2)}
          </Text>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 0,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
});
