\documentclass{article}
\usepackage{minted}
\usepackage{hyperref}
\usepackage[utf8]{inputenc}
\usepackage[pdftex]{graphicx}
\graphicspath{./}
\DeclareGraphicsExtensions{.png}

\title{App Controlled Vehicle}
\author{Mindaugas Sarskus \& Elena Makarenko \\  
\\
\url{https://gitlab.com/MakarenkoElena91/gbu}}

\date{March 2020}

\begin{document}

\maketitle

\section{ General information }
% Our idea for the project was to control Initio\cite{initio-robot} robot car by using phone build in sensors. There were a few ideas how we could achieve this and we tried some of them through the course of the semester. We used GitLab board feature to document our work as much as we could \cite{project-repo-boards}

Our project idea is to control Initio\cite{initio-robot} robot car by using phone built in orientation sensors. There were a few ideas how we could achieve that. Although, some implementations of those ideas were not successful, a significant amount of research has been done which you can find documented in the GitLab Issue Board. \cite{project-repo-boards}.

\section{ Usage } 
In order to test our final project implementation you will need to set up both hardware and software.

\subsection{Hardware Setup}
The following hardware is required:
\begin{itemize}
	\item A car which has two motors: one for left and one for right side. In our project we used Initio\cite{initio-robot} robot car chassis.
	\item Raspberry Pi (we used 3 model) \cite{rpi-website} which is used as a web server in order to control the robot. 
	\item A Dual H Bridge \cite{h-bridg-wiki} for motor control. Please see wiring diagram for our setup Fig.\ref{fig:robo-car-wiring-diagram}
	\item Mobile phone or tablet with a browser installed (we used Android phone with Chrome).
	Please check if your browser is compatible with Sensor APIs\cite{web-sensors-api}. See Fig.\ref{fig:browserSupport}
\end{itemize}

\subsection{Software Setup}
After hardware is assembled and all wiring is complete you will also need:
\begin{itemize}
    \item Raspbian OS\cite{raspbian-os} to be installed on SD card for Raspberry PI to boot. We were using Raspbian OS Lite version.
    \item Node.js installed on Raspberry Pi.  
    \item Clone our git repository\cite{project-git-repo} to the Raspberry Pi. 
    \item Install all dependencies and start Express web server inside Raspberry PI.
    \item Open browser on your phone and enter RPi ip address.
\end{itemize}

\subsection{UI and Usage}

At this point you should have:
\begin{itemize}
    \item Robot-car assembled and powered.
    \item Raspberry Pi running and all required software installed.
    \item If everything is set up correctly you should see our simple yet informative UI running in the phone's browser. Figure \ref{fig:ui-phone-landscape}.
\end{itemize}

 If browser has access to phone sensors you should see blue dot moving as you move the phone (Figure \ref{fig:ui-phone-landscape}).  The ball's position is mapped directly to the robot-car motors, if ball is in the centre then car stays still. So be careful when connecting browser to the car as it might start to move suddenly. \textbf{Keep phone flat to stop the robot-car}.
  
\begin{figure}[ht]
    \centering
    \includegraphics[width=0.7\textwidth]{img/ui-phone-landscape.jpeg}
     \caption{Web UI on the mobile phone Chrome browser}
    \label{fig:ui-phone-landscape}
\end{figure}

First see Figure \ref{fig:yaw-pitch-roll} in order to understand how to control the robot-car. 
Hold your phone the same way you would hold a plane on that figure (its nose is in opposite direction from you as if you are holding its wings).

You need to hold the phone in a landscape mode and for the best experience we recommend you to lock the screen in that mode.
This is an initial position in which car stays still or stops if in motion.
By tilting the phone forward/backward or/and left/right, car moves forward/backward or/and left/right accordingly. 

Note: you might need to refresh browser if UI does not fill the whole screen. Our project goal was not to build a perfect UI.

% We use \textbf{roll} to turn left/right and \textbf{pitch} for acceleration and direction.
The angle at which phone is tilted represents the power which is applied to the motors. The more you tilt the more power is applied, the faster the speed. The blue dot on the screen provides a visual feedback about phone tilt positions.

 The light yellow area in the centre represents tilt angles at which robot car will not drive but rather spin on the spot. We believe this way of controlling robot-car is quite intuitive which was proven while testing. 

\section{ Design Decisions and Issues } 

\subsection{Web Sensors API - Original Idea}
Our original idea was to build a web server on a Raspberry PI which would host web page and would make use of Sensors API \cite{web-sensors-api}.
Even idea is quite ambitious and involves relatively new technologies (approximately 5 years old) we managed to achieve most of our goals.
% We were also planning to turn it into a Progressive Web App (PWA)\cite{pwa-wiki}. Thus we could allow any device with compatible browser become robot-car controller. No installation would be needed, just open browser enter robot-car ip and you are ready to go. Could not be simpler, well... actually it can. PWA allows web site to be installed with offline functionality. By making our web site PWA user could install robot-car controller on their device. Next time all you need is just to click the icon and it would look like a native app. Moreover, one of the advantages of PWA is that its startup speed is faster compared to native apps.

\begin{figure}[ht]
    \centering
    \includegraphics[width=0.5\textwidth]{img/lighthouse-score.jpeg}
     \caption{Lighthouse test results}
    \label{fig:lighthouse-score}
\end{figure}

At the very start, we found a promising example by Intel Generic Sensor API \cite{intel-generic-sensors} but at the end we used only one file from it which was the Intel implementation for the Sensors API interface. Intel examples are hosted on GitHub pages over HTTPS. 
In order to skip this overhead related to HTTPS setup we decided to abandon this idea and try to create a React Native application, but as we figured out later new versions of Android do not support http as well\cite{react-native-no-http}.
Finally, we concluded that it is better to come back to the initial idea.
The main argument for that idea was the fact that  web application can be run on
any phone, without installing any additional software.
Apart from that it can be converted to a Progressive Web App (PWA).
PWA allows a web site to be installed with offline functionality. After installing PWA the next time you use the app, all you need is just to click the icon and it will look just like a native app. Moreover, one of the advantages of PWA is that its startup speed is faster compared to native apps.
Therefore we kept working on the first idea, trying to solve the problem with certificates.
We solved issue partially, by using a self signed certificate. This took big portion of our development time. HTTPS is required by Sensors API and PWA. As we use self signed certificate browser warns you about the untrusted connection with web server. This is the reason why browsers block our web site PWA feature (see Figure \ref{fig:pwa-concole-err}), even though we had quite a good score in Lighthouse test (see (Figure\ref{fig:lighthouse-score}). We found other solutions to our HTTPS certificate issue but their implementation was beyond our time constrains and scope of this project.
\begin{figure}[ht]
    \centering
    \includegraphics[width=0.9\textwidth]{img/pwa-concole-err.jpeg}
     \caption{PWA error logged to console}
    \label{fig:pwa-concole-err}
\end{figure}
\subsubsection{Architecture}  

 \begin{figure}[h!]
    \centering
    \includegraphics[width=0.6\textwidth]{img/gesture-projec-diagram.png}
     \caption{Project Diagram\cite{aircraft-principal-axes}}
    \label{fig:project-diagram}
\end{figure}


In this project we decided to use 'Edge Computing' ('Fat Client') architecture.
In this type of architecture data is processed by the device itself in order to reduce the load on the server and to improve response time. As our web server is running on the Raspberry Pi which has limited computing power this type of architecture suits the most. Consequently, all the logic is implemented on the client side. Data is read by phone sensors and only after all the computations are done, the output of these computations is sent as a request to the server. Server maps the received data to the robot-car motors spinning direction and power (Fig \ref{fig:project-diagram}). Request data that is sent contains information about the power that should be applied to the left and right motors. Negative power means motor should spin in reverse direction.

\subsubsection{Roll \& Pitch Axes} 
There are 2 axes that we use:
\begin{enumerate}
    \item Transverse axis (pitch)
    \item Longitudinal axis (roll)
\end{enumerate}

\begin{figure}[ht]
    \centering
    \includegraphics[width=0.6\textwidth]{img/yaw-pitch-roll.png}
     \caption{Yaw Pitch Roll\cite{aircraft-principal-axes}}
    \label{fig:yaw-pitch-roll}
\end{figure}

'Pitch' values are responsible for the car's speed and direction. The higher the value the faster the speed. Depending on the value being positive or negative the car moves either forward or backward. Accordingly, if the value is close to 0 vehicle either stops or does not move.

'Roll' values are responsible for car turning left or right. If value is negative, speed of left wheels is reduced to turn left. If value is positive speed of right wheels is reduced to turn right. Accordingly, if value is close to initial value car does not turn, keeps driving straight.

Requests are sent only if the difference between the value of the previous request and the current value is greater than a certain value in order to minimize load on the server. Moreover, slight changes in values will not really affect car movement.

\subsubsection{Web Sensors API}

In order to access phone's sensors in browser we used ApsoluteOrientationSensor API \cite{absolutSensor}. It does not have full support for all browsers yet, but we believe it is just a matter of time. The compatibility table is provided below (Fig \ref{fig:browserSupport}).  

\begin{figure}[ht]
    \centering
    \includegraphics[width=0.9\textwidth]{img/browserSupport.png}
     \caption{Browser support for web sensors}
    \label{fig:browserSupport}
\end{figure}

% Finally, we concluded that it is better to come back to the initial idea. The main argument for that idea was the fact that  web application can be run on any phone, without installing any additional software. Apart from that it can be converted to a progressive web app. Therefore we kept working on the first idea, trying to solve the problem with certificates.

% Although, there is at least one certification authority that provides certificates for free, we decided to use self-signed certificate due to time constraints. As a result, user needs to accept the certificate in order to use the app.



\subsubsection{Fuzzy Logic Control}  

While developing the app we decided to add fuzzy logic in order to make the control smoother. Idea came even before we had the first run, but it seemed to be the right decision which would be beneficial for our project.

% felt like right decision as fuzzy logic was mentioned during Gesture lectures and During AI lecture we examined couple fuzzy rule examples which felt could be adopted to our project.
As our project stack was using Node.js, JavaScript (JS) runtime for the server side, we needed library which is compatible with JS. We found very few of them, but they were limited or written in other languages and involved dark voodoo magic to make it work with JS. 

While researching fuzzy libraries available online we found es5-fuzzy\cite{es6-fuzzy} library. Even it was limited to our needs but it was written in JS, source code was available on GitHub and it was easy to understand. After we tried it and investigated its limits Mindaugas decided to give a go to upgrade it to our needs. After a few hours of brainstorming we came up with list of rules. One day later new fuzzy library born which was almost complete rewrite of es6-fuzzy. Bellow you can find the code snippets and comments that explain the changes that were made.

\begin{minted}[mathescape, linenos]{js}
// es6-fuzz had no fuzzy sets so each item was declared separately
const harmlessWeapon = new Fall(20, 60)
const dangerousWeapon = new Triangle(20, 50, 80)
const lethalWeapon = new Rise(40, 80
// ...

// es6-fuzz  had no continent way do describe fuzzy rules
// eg: RULE_1 (50%, 65%)
// If (weapon) is slightly [lethal]
// And (enemy) extremely [formidable]
// Then (damage) is [great]
const fuzzLeathal = lethalWeapon.fuzzify(WEAPON)
const fuzzFormida = formidable.fuzzify(ENEMY)
// AND = min
const fuzz1 = Math.min(fuzzLeathal, fuzzFormida)
// get rule value
const rule1 = fuzz1 * great.fuzzify()
console.log(rule1)
// ...

// es6-defuzzier was needed code manually
const defuzz = (rule1 + rule2 + rule3) / (fuzz1 + fuzz2 + fuzz3)

\end{minted}

\begin{minted}[mathescape, linenos]{js}
// fuzzy set declaration
const weaponFuzzySet = new FuzzySet('weapon',
    new FuzzySetEntry({entryName: 'harmless',  shape: new Fall(20, 60)}),
    new FuzzySetEntry({entryName: 'dangerous', shape: new Triangle(20, 50, 80)}),
    new FuzzySetEntry({entryName: 'lethal',    shape: new Rise(40, 80) }),
)
//...

// fuzzy rules declaration (support hedges too)
// RULE_1
// If (weapon) is <slightly> [lethal]
// And (enemy) is <extremely> [formidable]
// Then (damage) is [great]
const rule_1 = new FuzzyRule()
rule_1
    .If('weapon', weaponFuzzySet.getFuzzyfier('lethal'), Hedges.slightly)
    .And('enemy', enemyFuzzySet.getFuzzyfier('formidable'), Hedges.indeed)
    .Then('damage', damageFuzzySet.getFuzzyfier('great'))
//...

// using defuzzifier
const sugeno = new Sugeno(rule_1, rule_2, rule_3)
const result = sugeno.defuzzify(50, 65)
\end{minted}

It is unarguable ease of use improvement. 
Our original idea was to use fuzzy logic to reduce speed of motor in which direction robot-car is moving. So turns appear smooth. But after first robot-car run it was obvious that instead of reducing speed, which made user turn its phone more, we should boost values so user could have more control with less phone movements. 

Although our fuzzy rules are not perfect and require more time to fine tune them, still they work and do the job. In perfection appears then value are close to the max as blue dot at UI jumps a bit, but it has no effect on robot-car control as it is already at max speed and small power jumps doesn't effect it. All fuzzy logic calculations are done on user device.
 
\subsection{Hardware - Initio robot-car}
\begin{figure}[ht]
    \centering
    \includegraphics[width=0.5\textwidth]{img/wireing-diagram.png}
     \caption{Robot-Car Wiring diagram. Ignore battery values they are for demonstration purposes}
    \label{fig:robo-car-wiring-diagram}
\end{figure}
During the hardware implementation we had an accident after which Initio motor control board stopped working. It happened because we did not have proper power supply, and the one that was provided was not powerful enough. So Mindaugas used his own one which has right voltage specs but specs were way off from actual voltage it was providing. We didn't want to use batteries as they discharge quickly. As it was Mindaugas responsibility to check voltage, he ordered new one to replace the old one (he insisted to take responsibility). Because we currently surviving COVID-19 apocalyptic we didn't want to risk and wait for delivery, but instead build our own motor controller using Dual H Bridge module Mindaugas had (fig \ref{fig:dual-h-bridge-pinout}).

\begin{figure}[ht]
    \centering
    \includegraphics[width=0.8\textwidth]{img/dual-h-bridge-pinout.jpg}
     \caption{Dual H Bridge}
    \label{fig:dual-h-bridge-pinout}
\end{figure}

Another design decision we made is to attach separate power bank for the Raspberry PI, that way we were able to prevent raspberry from restarting as sometimes power drops below the required value when motors are started. So our design has 2 power banks: one for RPI and one for the robot-car motors (fig. \ref{fig:robo-car-wiring-diagram}).
\begin{figure}[hb]
    \centering
    \includegraphics[width=0.7\textwidth]{img/GPIO.png}
     \caption{GPIO}
    \label{fig:GPIO}
\end{figure}
\subsubsection{Software - rpio} 

In order to control  GPIO (general-purpose input/output) pins, a research was conducted and we brought in a decision in favour of rpio npm\cite{rpio-npn} library mainly because it is widely used and has support for the PWM pin mode. 

As mentioned before requests are only send if the values' changes are noticeable. This prevents the server from overloading.

\section{Conclusion}  
This project was quite a challenging journey for us. 
Firstly, it involved a lot of research work and use of new technologies such as Web Sensors API and PWA. Secondly, working with hardware is always a challenge. During the project time we made some critical decisions such as move to React Native and then go back to Web Sensors API, as use our own motor control board instead of waiting for delivery and hope for the best. It even involved creation of fuzzy logic library which might be published to npm if we find time to polish code a bit.

If we had more time to do this project we believe we wouldn't change much. We would improve it by adding HTTPS without self signed cert and by better polished Fuzzy Logic and UI. If we still have time left we would try to add array of ultrasonic distance sensors. That way we could add functionality to avoid obstacles. Actually, we tried it at early stage and made sensor array but abandoned this idea because of time constrains. 

\section{Note: References} 
As we wanted to get practice for main project dissertation we wrote this document in Latex. But for some reason compiled page doesn't show the links. All the links and references are added to our Git repository.

 \bibliographystyle{ieeetr}
  \bibliography{bibliography}
\end{document}
