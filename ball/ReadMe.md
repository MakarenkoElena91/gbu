A simple UI for controlling the ball on the screen written in plain javascript.
The HTML <canvas> element is used to draw graphics on a web page.
x and y coordinates represent pitch and roll values.
Light yellow area represent absence of speed, just turning on the spot.