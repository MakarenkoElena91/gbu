let canvas = document.getElementById("myCanvas")
let ctx = canvas.getContext("2d")
ctx.canvas.width = window.innerWidth - 50 * 2
ctx.canvas.height = window.innerHeight - 50 * 2
let ballRadius = 30

const X = ctx.canvas.width / 2
const Y = ctx.canvas.height / 2
// ball's initial position
let ballX = X
let ballY = Y
console.log("clicked ", ballX, ballY)

function draw_axis() {
    ctx.strokeStyle = '#000000'
    ctx.lineWidth = 5
    //vertical y
    ctx.beginPath();
    ctx.moveTo(ctx.canvas.width / 2, 0)
    ctx.lineTo(ctx.canvas.width / 2, ctx.canvas.height)
    ctx.stroke()

    //horizontal x
    ctx.beginPath()
    ctx.moveTo(0, ctx.canvas.height / 2)
    ctx.lineTo(ctx.canvas.width, ctx.canvas.height / 2)
    ctx.stroke();
}

function draw_extra_lines() {
    ctx.strokeStyle = '#000000'
    ctx.lineWidth = 1
    ctx.beginPath()

    ctx.moveTo(ctx.canvas.width / 4, 0);
    ctx.lineTo(ctx.canvas.width / 4, ctx.canvas.height)
    ctx.stroke();

    ctx.beginPath()
    ctx.moveTo(ctx.canvas.width * 3 / 4, 0);
    ctx.lineTo(ctx.canvas.width * 3 / 4, ctx.canvas.height)
    ctx.stroke()

    ctx.beginPath()
    ctx.moveTo(0, ctx.canvas.height / 4)
    ctx.lineTo(ctx.canvas.width, ctx.canvas.height / 4)
    ctx.stroke()

    ctx.beginPath()
    ctx.moveTo(0, ctx.canvas.height * 3 / 4)
    ctx.lineTo(ctx.canvas.width, ctx.canvas.height * 3 / 4)
    ctx.stroke()
}

function drawBall({x = X, y = Y}) {
    ctx.clearRect(0, 0, canvas.width, canvas.height)
    draw_axis()
    draw_extra_lines()
    showValues()
    ctx.beginPath()
    ctx.arc(x, y, ballRadius, 0, Math.PI * 2)
    ctx.fillStyle = "#0095DD"
    ctx.fill()
    ctx.closePath()
}

function sendValues(x, y) {
    console.log("x & y ", x, y)
    let percentageX = (x / 100)
    let percentageY = (y / 100)
    console.log("percentageX & percentageY ", percentageX, percentageY)

    ballX = X + X * percentageX
    ballY = Y - Y * percentageY

    console.log("ballX ballY ", ballX, ballY)

    draw_axis()
    draw_extra_lines()
    drawBall({x: ballX, y: ballY})
}

function showValues() {
    let xPosition = 120

    let x = ballX - X
    let y = ballY + Y

    let relX = document.getElementById('x').value;
    let relY = document.getElementById('y').value;
    console.log(x);
    ctx.font = "30px Arial";
    ctx.fillText("Abs x =", 10, 50)
    ctx.fillText(x, xPosition, 50)
    ctx.fillText("Abs y =", 10, 100)
    ctx.fillText(y, xPosition, 100)


    ctx.fillText("Rel x =", 10, 150)
    ctx.fillText(relX, xPosition, 150)
    ctx.fillText("Rel y =", 10, 200)
    ctx.fillText(relY, xPosition, 200)
}


drawBall({})
