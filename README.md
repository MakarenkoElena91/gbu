# App Controlled Vehicle
This repository contains 4 projects. Each of them have its own `ReadMe.md`:
- `rpio-gpio-rest` contains our main project
- `fuzzy-test` contains fuzzy library we created.
- `client` contains React Native app we created then we planed to switch to it.
- `ball` contains our UI early stage.
- `Report` contains report Latex source code and compiled pdf of it.

## Table of contents
* [General information](#general-information)
* [Usage](#usage)
* [Design Decisions and Issues](#design-decisions-and-issues)
  * [Client](#client)
  * [Server](#server)
* [Future improvements](#future-improvements)
* [Reference](#reference)

## General information  
Idea of the project is controlling a car by using a phone.
Phone accesses 3 of its sensors: Accelerometer, Gyroscope and Magnetometer,
the combined data from which is used to control the car.

## Usage
A user holds the phone in landscape orientation, face up, perpendicular to floor.
This is the initial position in which car stays still or stops if in motion.
By tilting the phone forward/backward or/and left/right, car moves
forward/backward or/and left/right accordingly.

## Design Decisions and Issues
### Client
##### Architecture   
In this project we decided to use 'Edge Computing' architecture. In this type of
architecture data is processed by the device itself in order to improve response
times and save bandwidth.
Consequently, all the logic is implemented on the client side.
Data is read by phone sensors and only after all the computations are done
the outputs of these computations are sent as a request to the server.
Request that is being sent is: speed, direction (forward/backward),
angle of turn and direction (left/right).

###### Roll & Pitch Axes
There are 2 axes that we use:
1. Transverse axis (pitch)
2. Longitudinal axis (roll)

[https://en.wikipedia.org/wiki/Aircraft_principal_axes][1]

'Pitch' values are responsible for the car's speed and direction. The higher the
value the faster car goes. Depending on the value being positive or negative the
car moves  forwards or backwards. Accrordingly, if the value is close to 0 vehicle
either stops or does not move.

Roll values are responsible for car turning left or right If  value is negative,
speed of left wheels is reduced to turn left.
If value is positive speed of right wheels  is reduced to turn right.
Accrordingly, if value is close to initial value car does not turn,
keeps driving straight.

Requests are sent only if the difference between the value of the previous request
and the current value is greater than a  certain value (will decide later) in
order to minimize load on the server. Moreover, slight changes in values will not
really affect car movement.

In order to access phone's sensors in browser we used RelativeOrientationSensor
sensor API. It does not have full support for all browsers yet, but we think it
is just a matter of time. The compatibility table is provided below (fig.1).
ref: https://developer.mozilla.org/en-US/docs/Web/API/RelativeOrientationSensor

Originally, the idea was to use web sensors by adapting intel Generic Sensor API:
ref: https://github.com/intel/generic-sensor-demos
REST architecture required secure https protocol. In order to conduct communication
between client and server over https a certificate is required. In order to
skip this overhead we decided to abandon this idea and try to create a React
ative application, but new versions of Android do not support http as well.
[https://github.com/facebook/react-native/issues/24408#issuecomment-482123560][]

Finally, we concluded that it is better to come back to the initial idea.
The main argument for that idea was the fact that  web application can be run on
any phone, without installing any additional software.
Apart from that it can be converted to a progressive web app.
Therefore we kept working on the first idea, trying to solve the problem with
certificates.

Although, there is at least one certification authority that provides certificates
for free, we decided to use self-signed certificate due to time constraints.
As a result, user needs to accept the certificate in order to use the app.

###### Fuzzy Logic Control
We attempted to implement fuzzy logic in order to adjust speed when
turning. Two values which are absolute values of speed and angle of turn are the
inputs to the fuzzy logic. The output is a coefficient which will be multiplied
further.

When searching for the libraries for fuzzy logic control we have realized that
there are not so many libraries that are written in javascript. Thus, we decided
to rewrite the implementation of the fuzzylite library
as it was quite basic and did not meet our needs and requirements.
Although, it was stil simpler to extend in comparison to some others.
ref:https://github.com/sebs/node-fuzzylogic
We used Sugeno fuzzy inference system as it is  more computationally efficient:
both fast and accurate enough.

### Server

###### Software (Raspberry PI & Node Express)

In order to control  GPIO (general-purpose input/output) pins,
a research was conducted and we brought in a decision in favor of rpio-npm.
Firstly, because the framework for backend that we have chosen is node express
(the reason for that was the fact that we had some experience before and were
quite familiar with it).
Secondly, it is the one that has PWM (pulse-width modulation).
The other options we looked at are: on-off, rp -gpo, etc.


## Future Improvements

1. UI
2. Fuzzy Logic
3. Distance sensors

## Videos
- [How to control](https://vimeo.com/402525654)
- [First test drive](https://vimeo.com/402525719)
- [Hardware](https://vimeo.com/402526436)

## Reference
[Aircraft_principal_axes][1]     
[express-gpio-rest-api][2]  
[RelativeOrientationSensor][3]  
[Generic-sensor-demos][4]  
[Rpio][5]  
[Raspberrypi][6]  
[Issue1][7]  

[1]: https://en.wikipedia.org/wiki/Aircraft_principal_axes  
[2]: https://github.com/juangesino/express-gpio-rest-api    
[3]: https://developer.mozilla.org/en-US/docs/Web/API/RelativeOrientationSensor  
[4]: https://github.com/intel/generic-sensor-demos   
[5]: https://www.npmjs.com/package/rpio  
[6]: https://www.raspberrypi.org/documentation/usage/gpio/  
[7]: https://github.com/facebook/react-native/issues/24408#issuecomment-482123560  
