const Logic     = require('./es6-fuzz/lib/logic')
const Trapezoid = require('./es6-fuzz/lib/curve/trapezoid')
const Fall      = require('./es6-fuzz/lib/curve/reverse-grade')
const Rise      = require('./es6-fuzz/lib/curve/grade')
const Triangle  = require('./es6-fuzz/lib/curve/triangle')
const Constant  = require('./es6-fuzz/lib/curve/constant')

// RULE_1
// If (weapon) is slightly [lethal]
// And (enemy) extremely [formidable]
// Then (damage) is [great]

// weapon
const harmlessWeapon = new Fall(20, 60)
const dangerousWeapon = new Triangle(20, 50, 80)
const lethalWeapon = new Rise(40, 80)

// enemy
const weak = new Triangle(0, 20, 40)
const strong = new Triangle(30, 50, 70)
const formidable = new Triangle(60, 80, 100)

// damage
const small = new Constant(20)
const partial = new Constant(50)
const great = new Constant(80)

const data = {WEAPON: 50, ENEMY: 65}
const {WEAPON, ENEMY} = data

// RULE_1 (50%, 65%)
// If (weapon) is slightly [lethal]
// And (enemy) extremely [formidable]
// Then (damage) is [great]

// fuzzyfy
const fuzzLeathal = lethalWeapon.fuzzify(WEAPON)
const fuzzFormida = formidable.fuzzify(ENEMY)
// AND = min
const fuzz1 = Math.min(fuzzLeathal, fuzzFormida)
// get rule value
const rule1 = fuzz1 * great.fuzzify()
console.log(rule1)


// RULE_2 (50%, 65%)
// If (weapon) is very [dangerous]
// And (enemy) is indeed [strong]
// Then (damage) is [partial]
const fuzzDangerous = dangerousWeapon.fuzzify(WEAPON)
const fuzzStrong = strong.fuzzify(ENEMY)
const fuzz2 = Math.min(fuzzDangerous, fuzzStrong)
const rule2 = fuzz2 * partial.fuzzify()
console.log(rule2)

// RULE_3 (50%, 65%)
// If (weapon) is more less [harmless]
// Or (enemy) is somewhat [weak]
// Then (damage) is very very[small]
const fuzzHarmless = harmlessWeapon.fuzzify(WEAPON)
const fuzzWeek = weak.fuzzify(ENEMY)
const fuzz3 = Math.max(fuzzHarmless, fuzzWeek)
const rule3 = fuzz3 * small.fuzzify()
console.log(rule3)

const defuzz = (rule1 + rule2 + rule3) / (fuzz1 + fuzz2 + fuzz3)
console.log('Defuzz: ', defuzz)



const logic_1 = new Logic()
const res = logic_1
    .If('cold', new Trapezoid(0, 12, 18, 20)) // until 12-18 around warm
    .Or('warm', new Trapezoid(12, 14, 16, 70)) // until 12-18 around warm
    .And('hot', new Rise(60, 80)) // until 12-18 around warm
    .Then('mood', new Constant(3))
    .defuzzify(10, 20, 79)

// flow
// const logic2 = new Logic()
// const res2 = logic2
//     .init('slow', new Fall(0, 5))
//     .or('medium', new Triangle(4, 7, 10))
//     .or('fast', new Rise(9, 12))
//     .defuzzify(9.5)



console.log(res)
// console.log(res2)
