const Logic = require('es6-fuzz/lib/logic')
const Trapezoid = require('es6-fuzz/lib/curve/trapezoid')
const Fall = require('es6-fuzz/lib/curve/reverse-grade')
const Rise = require('es6-fuzz/lib/curve/grade')
const Triangle = require('es6-fuzz/lib/curve/triangle')
const Constant = require('es6-fuzz/lib/curve/constant')


// const data = {PITCH: 1, ROLL: 3}
const data = {PITCH: 0.5, ROLL: 1}
const {PITCH, ROLL} = data

// Pitch graph
const smallPitch = new Fall(0.2, 0.4)
const mediumPitch = new Triangle(0.3, 0.5, 0.7)
const largePitch = new Rise(0.6, 0.8)

// Roll graph
const smallRoll = new Fall(0.15, 0.25)
const mediumRoll = new Triangle(0.2, 0.3, 0.4)
const largeRoll = new Rise(0.35, 0.5)

// Reducer
const smallReducer = new Constant(10)
const averageReducer = new Constant(50)
const bigReducer = new Constant(90)


// fuzzy holders
const fuzzes = []

/*
    Rule 1:
        If PITCH is small
        And ROLL is small
        Then REDUCER is big
 */
const fuzzSmallPitch = smallPitch.fuzzify(PITCH)
const fuzzSmallRoll = smallRoll.fuzzify(ROLL)
const fuzz = Math.min(fuzzSmallPitch, fuzzSmallRoll)
const rule = fuzz * bigReducer.fuzzify()
fuzzes.push({fuzz, rule})
console.log('rule 1: ', fuzzes[0])

/*
    Rule 2:
        If PITCH is medium
        And ROLL is medium
        Then REDUCER is average
 */
const fuzzMediumPitch = mediumPitch.fuzzify(PITCH)
const fuzzMediumRoll = mediumRoll.fuzzify(ROLL)
const b = {}
b.fuzz = Math.min(fuzzMediumPitch, fuzzMediumRoll)
b.rule = b.fuzz * averageReducer.fuzzify()
fuzzes.push(b)
console.log("rule 2: ", b)

/*
    Rule 3:
        If PITCH is large
        And ROLL is large
        Then REDUCER is small
 */
const fuzzLargePitch = largePitch.fuzzify(PITCH)
const fuzzLargeRoll = largeRoll.fuzzify(ROLL)
const c = {}
c.fuzz = Math.min(fuzzLargePitch, fuzzLargeRoll)
c.rule = c.fuzz * smallReducer.fuzzify()
fuzzes.push(c)
console.log("rule 3: ", c)

// get awesome fuzzzzz
const fuzzSum = fuzzes.map(o => o.rule).reduce((a, b) => a + b)
const divider = fuzzes.map(o => o.fuzz).reduce((a, b) => a + b)

console.log('answ: ', fuzzSum / divider)

/*
    Rules:
        If PITCH is [little] And ROLL is [little]  Then REDUCE [little]  - slight hand movements
        If PITCH is [little] And ROLL is [average] Then REDUCE [little]
        If PITCH is [large]  Or  ROLL is [large]   Then REDUCE [little] - phone shake, drop
 */
