const Trapezoid = require('./lib/curve/trapezoid')
const Fall      = require('./lib/curve/reverse-grade')
const Rise      = require('./lib/curve/grade')
const Triangle  = require('./lib/curve/triangle')
const Constant  = require('./lib/curve/constant')

const FuzzySet  = require('./lib/FuzzySet')
const FuzzySetEntry = require('./lib/FuzzySetEntry')
const FuzzyRule = require('./lib/FuzzyRule')
const Hedges    = require('./lib/hedges').Hedges
const Sugeno    = require('./lib/DefuzzifierSugeno')


/////////////////////////////////////

const weaponFuzzySet = new FuzzySet('weapon',
    new FuzzySetEntry({
        entryName: 'harmless',
        shape: new Fall(20, 60)
    }),
    new FuzzySetEntry({
        entryName: 'dangerous',
        shape: new Triangle(20, 50, 80)
    }),
    new FuzzySetEntry({
        entryName: 'lethal',
        shape: new Rise(40, 80)
    }),
)

const enemyFuzzySet = new FuzzySet('enemy',
    new FuzzySetEntry({
        entryName: 'weak',
        shape: new Triangle(0, 20, 40)
    }),
    new FuzzySetEntry({
        entryName: 'strong',
        shape: new Triangle(30, 50, 70)
    }),
    new FuzzySetEntry({
        entryName: 'formidable',
        shape: new Triangle(60, 80, 100)
    }),
)

const damageFuzzySet = new FuzzySet('damage',
    new FuzzySetEntry({
        entryName: 'small',
        shape: new Constant(20)
    }),
    new FuzzySetEntry({
        entryName: 'partial',
        shape: new Constant(50)
    }),
    new FuzzySetEntry({
        entryName: 'great',
        shape: new Constant(80)
    }),
)

console.log(weaponFuzzySet.toString())
console.log(enemyFuzzySet.toString())
console.log(damageFuzzySet.toString())
// console.log(weaponFuzzySet)

// RULE_1
// If (weapon) is <slightly> [lethal]
// And (enemy) is <extremely> [formidable]
// Then (damage) is [great]
const rule_1 = new FuzzyRule()
rule_1
    .If('weapon', weaponFuzzySet.getFuzzyfier('lethal'), Hedges.slightly)
    .And('enemy', enemyFuzzySet.getFuzzyfier('formidable'), Hedges.indeed)
    .Then('damage', damageFuzzySet.getFuzzyfier('great'))

// RULE_2 (50%, 65%)
// If (weapon) is very [dangerous]
// And (enemy) is indeed [strong]
// Then (damage) is [partial]
const rule_2 = new FuzzyRule()
rule_2
    .If('weapon', weaponFuzzySet.getFuzzyfier('dangerous'), Hedges.very)
    .And('enemy', enemyFuzzySet.getFuzzyfier('strong'), Hedges.indeed)
    .Then('damage', damageFuzzySet.getFuzzyfier('partial'))

// RULE_3 (50%, 65%)
// If (weapon) is more less [harmless]
// Or (enemy) is somewhat [weak]
// Then (damage) is very very[small]
const rule_3 = new FuzzyRule()
rule_3
    .If('weapon', weaponFuzzySet.getFuzzyfier('harmless'), Hedges.moreless)
    .Or('enemy', enemyFuzzySet.getFuzzyfier('weak'), Hedges.somewhat)
    .Then('damage', damageFuzzySet.getFuzzyfier('small'), Hedges.very, Hedges.very)

console.log(rule_1.toString())
console.log(rule_2.toString())
console.log(rule_3.toString())
// console.log(rule_1)

// const fuzz1 = rule_3.fuzzify(50, 65)
// console.log(fuzz1)

//////////////////////////////////////////////////////////

const sugeno = new Sugeno(rule_1, rule_2, rule_3)
const result = sugeno.defuzzify(50, 65)

console.log(result)



