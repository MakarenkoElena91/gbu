'use strict'

const ruleEngine = {
    if:  (a, b)  => a,
    and: (a, b) => Math.min(a, b),
    or:  (a, b) => Math.max(a, b),
    not: (a, b) => 1 - a,
    then:(a, b) => a,
}

class FuzzyRule {
    constructor() {
        this.rules = []
    }

    If(noun, fuzzifier, ...hedges) {
        const rule = ruleEngine.if
        this.rules.push({noun, hedges, fuzzifier, rule})
        return this
    }

    And(noun, fuzzifier, ...hedges) {
        const rule = ruleEngine.and
        this.rules.push({noun, hedges, fuzzifier, rule})
        return this
    }

    Or(noun, fuzzifier, ...hedges) {
        const rule = ruleEngine.or
        this.rules.push({noun, hedges, fuzzifier, rule})
        return this
    }

    Not(noun, fuzzifier, ...hedges) {
        const rule = ruleEngine.not
        this.rules.push({noun, hedges, fuzzifier, rule})
        return this
    }

    Then(noun, fuzzifier, ...hedges){
        const rule = ruleEngine.then
        this.rules.push({noun, hedges, fuzzifier, rule})
        return this
    }

    fuzzify(...values){
        if(values.length !== this.rules.length -1){
            const err = `Needed values ${this.rules.length -1 }, but got: ${values.length}. (${values}, ${this.rules})`
            throw err
        }

        let fuzz = values
            .map((value, index) => {
                const rule = this.rules[index]
                const fuzzVal = rule.fuzzifier.fuzzify(value)
                const obj = { fuzzVal, ...rule}
                // console.log(obj)
                return obj
            })
            .map(obj => {
                for(let f of obj.hedges){
                    // console.log(f)
                    // console.log(obj)
                    obj.fuzzVal = f(obj.fuzzVal)
                }
                // console.log(obj)
                return obj
            })
            .reduce((a, b ) => {
                const value =  b.rule(a.fuzzVal, b.fuzzVal)
                // console.log(b.rule)
                return value
            })

        const thenHedges = this.rules[this.rules.length -1].hedges
        for(let h of thenHedges){
            fuzz = h(fuzz)
        }

        // console.log(fuzz)
        return fuzz
    }

    getThenRuleShape(){
        const thenRule = this.rules[this.rules.length -1]
        return thenRule.fuzzifier.shape
    }

    toString() {
        return '"'+ this.rules
            .map(rule => {
                let s = []
                s.push(rule.rule.name.capitalize())
                s.push(rule.noun)
                s.push(`is${(rule.hedges ? ' '+ rule.hedges.map(hedge => hedge.name).join(' ') : '')}`)
                s.push(rule.fuzzifier.entryName)
                return s.join(' ')
            })
            .join(' ')
        +'"'
    }
}

// https://stackoverflow.com/a/3291856/5322506
String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}

module.exports = FuzzyRule
