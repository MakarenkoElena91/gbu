exports.Hedges  = {
    extremely:  (n) => Math.pow(n, 3),
    very:       (n) => Math.pow(n, 2),
    slightly:   (n) => Math.pow(n, 1.7),
    little:     (n) => Math.pow(n, 1.3),
    identity:   (n) => n,
    indeed:     (n) => Math.pow(n, 2) * 2,
    moreless:   (n) => Math.sqrt(n),
    somewhat:   (n) => Math.cbrt(n),
}
