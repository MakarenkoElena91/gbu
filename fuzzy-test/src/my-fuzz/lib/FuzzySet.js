'use strict'
const FuzzySetEntry = require('./FuzzySetEntry.js')

/**
 *
 */
class FuzzySet {
    constructor(setName, ...entries) {
        this.setName = setName
        this.fuzzySet = new Map()
        entries.forEach(entry => {
            this.add(entry)
        })
    }

    add(entry){
        this.fuzzySet.set(entry.entryName, entry)
        return this
    }

    getFuzzyfier(fuzzifierName){
        const fuzzyfier = this.fuzzySet.get(fuzzifierName)
        if(!fuzzyfier){
            const err = `Fuzzifier not found for: ${fuzzifierName}`
            throw err
        }
        return fuzzyfier
    }

    toString(){
        const sets = [...this.fuzzySet.keys()].join(', ')
        return [this.setName.capitalize(), sets].join(': ')
    }
}


module.exports = FuzzySet

