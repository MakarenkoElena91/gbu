'use strict'
const Defuzzifier = require('./Defuzifier')
const FuzzyRule = require('./FuzzyRule')


class Sugeno extends Defuzzifier{

    defuzzify(...values){
        const fraction = this.rules
            .map(rule => {
                // console.log(rule.getThenRuleShape())
                rule.fuzzValue = rule.fuzzify(...values)
                // console.log(rule.getThenRuleShape())
                return rule
            })
            .map(rule => {
                // console.log(rule.fuzzifier)
                const nominator = rule.fuzzValue * rule.getThenRuleShape().cValue
                const denominator = rule.fuzzValue
                const fraction =  {nominator, denominator}
                // console.log(fraction)
                return fraction
            })
            .reduce((a, b) => {
                const nominator = a.nominator + b.nominator
                const denominator = a.denominator + b.denominator
                return {nominator, denominator}
            })

        return fraction.nominator / fraction.denominator
    }
}

module.exports = Sugeno
