# Fuzzy Test
This is project for testing and creating fuzzy library for the our project.

## Usage

Declaring fuzzy sets:
```js
const weaponFuzzySet = new FuzzySet('weapon',
    new FuzzySetEntry({entryName: 'harmless', shape: new Fall(20, 60)}),
    new FuzzySetEntry({entryName: 'dangerous', shape: new Triangle(20, 50, 80)}),
    new FuzzySetEntry({entryName: 'lethal', shape: new Rise(40, 80)}),
)
//... more sets
```

Fuuzy rule:
```js
// If (weapon) is <slightly> [lethal]
// And (enemy) is <indeed> [formidable]
// Then (damage) is [great]
const rule = new FuzzyRule()
    .If('weapon', weaponFuzzySet.getFuzzyfier('lethal'), Hedges.slightly)
    .And('enemy', enemyFuzzySet.getFuzzyfier('formidable'), Hedges.indeed)
    .Then('damage', damageFuzzySet.getFuzzyfier('great'))
//... more rules
```

Display the rule:
```js
console.log(rule.toString())
```
will log:
```bash
"If weapon is slightly lethal And enemy is indeed formidable Then damage is  great"
```

Run defuzzifyer:
```js
const sugeno = new Sugeno(rule_1, rule_2, rule_3)
const result = sugeno.defuzzify(50, 65)
```
