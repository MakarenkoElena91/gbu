# Raspberry Pi3 - GPIO - Rest

This project contains Gesture module submision code.

## Project structure:
- `app.js` - Node Express server main file.
- `routes/` - directory containing Express server routes.
- `public/` - directory contains files wich lill be hosted by the server. Eg. `index.html`

## Runing Project
To run this project first you need to have hardware setup.

#### Hardware
The following hardware is required:
- A car which has two motors: one for left and one for right side. In our project we used [Initio][1] robot car chassis.
- Raspberry Pi (we used 3 model) which is used as a web server as well to control robot itself.
- A [Dual H Bridge][2] for motor control. Please see wiring diagram for our setup.
- Mobile phone or tablet with a browser installed (we used Android phone with Chrome). Please check if your browser is compatible with Sensor API [here][3].

#### Software
- [Raspbian OS][4] to be installed on SD card for Raspberry PI to boot.
- Node.js installed on Raspberry Pi.
- You need to have `certs` to make HTTPS work. They are not provided.

#### Steps
- Clone this project in to Raspberry PI
- Instal Node packages: `npm install`. We using: [Express][5] and [rpio][6] packages.
- Run: `sudo node app.js`

If all went smoothly at this point you should have Express server running on Raspberry PI. Next you need conect you [compatible][3] device to this web server by entering `https://<<Raspbery Pi IP>>:3000` and you should see blue dot moving inside cartesian axis. Dependin on the balls location you robot-car shoud drive acordinaly.

## Notes
- Warning: Keep phone flat then connecting to server as it start sending request as soon as connects to it. 
- Server using HTTPS so you need add `certs` to meke it work. Self singht csrtificates are ok (we were using them) but browser warns about untrusted connection.
- To get Raspberry PI IP run: `ifconfig`
- Phone and Raspbery Pi should be connected on same network.
- We recomenr lock phone in landscape mode.






[1]: https://4tronix.co.uk/blog/?p=169
[2]: https://en.wikipedia.org/wiki/H_bridge
[3]: https://developer.mozilla.org/en-US/docs/Web/API/Sensor_APIs
[4]: https://www.raspberrypi.org/downloads/raspbian/
[5]: https://www.npmjs.com/package/express
[6]: https://www.npmjs.com/package/rpio
