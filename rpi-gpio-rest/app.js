const express = require('express')
const https = require('https')
const fs = require('fs')
const port = 3000
const path = require('path')
const cookieParser = require('cookie-parser')
const logger = require('morgan')

const indexRouter = require('./routes/index')
// const testRouter = require('./routes/test');
const motorRouter = require('./routes/motor-controll-rpio')

console.log(__dirname)

const key = fs.readFileSync(__dirname + '/../.cert/key.pem', 'utf8')
const cert = fs.readFileSync(__dirname + '/../.cert/cert.pem', 'utf8')

const options = {
    key: key,
    cert: cert
}

const app = express()

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({extended: false}))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))

app.use('/', indexRouter)
// app.use('/test', testRouter);
app.use('/motor', motorRouter)

// start https server
const server = https.createServer(options, app)

// // process.stdin.resume()
// const events = [
//     'exit',
//     'SIGINT',
//     'SIGUSR1',
//     'SIGUSR2',
//     'uncaughtException',
//     'SIGTERM'
// ]
// events.forEach((eventType) => {
//     process.on(eventType, () => {
//         motorRouter.stopMotors()
//         process.exit(0)
//     })
// })

server.listen(port, function () {
    console.log('Server Started on Port: 3000!')
})

