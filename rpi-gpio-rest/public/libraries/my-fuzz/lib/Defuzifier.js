'use strict'

/**
 * Abstract clas should be extended by all defuzifiers
 */
export class Defuzzifier {
    constructor(...rules) {
        this.rules = rules
    }

}
