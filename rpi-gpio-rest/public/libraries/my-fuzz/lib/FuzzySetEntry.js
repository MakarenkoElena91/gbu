'use strict'

/**
 * @example {entryName: 'average', fuzzifier: new Triangular(30, 50, 70)}
 */
export class FuzzySetEntry {
    constructor({entryName, shape}) {
        this.entryName = entryName
        this.shape = shape
    }

    fuzzify(value){
        return this.shape.fuzzify(value)
    }

    toString(){
        return `FuzzySetEntry: {name: ${this.entryName}, fuzzifier: ${this.shape}}`
    }
}
