'use strict'

/**
 *  @example
 *          const weaponFuzzySet = new FuzzySet('weapon',
 *               new FuzzySetEntry({entryName: 'harmless',  shape: new Fall(20, 60)}),
 *               new FuzzySetEntry({entryName: 'dangerous', shape: new Triangle(20, 50, 80)}),
 *               new FuzzySetEntry({entryName: 'lethal',    shape: new Rise(40, 80) }),
 *          )
 */
export class FuzzySet {
    constructor(setName, ...entries) {
        this.setName = setName
        this.fuzzySet = new Map()
        entries.forEach(entry => {
            this.add(entry)
        })
    }

    add(entry){
        this.fuzzySet.set(entry.entryName, entry)
        return this
    }

    getFuzzyfier(fuzzifierName){
        const fuzzyfier = this.fuzzySet.get(fuzzifierName)
        if(!fuzzyfier){
            const err = `Fuzzifier not found for: ${fuzzifierName}`
            throw err
        }
        return fuzzyfier
    }

    toString(){
        const sets = [...this.fuzzySet.keys()].join(', ')
        return [this.setName.capitalize(), sets].join(': ')
    }
}



