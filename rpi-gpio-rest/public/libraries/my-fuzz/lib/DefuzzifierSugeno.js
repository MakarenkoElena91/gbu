'use strict'

import {Defuzzifier} from './Defuzifier.js'


/**
 *  @example:
 *      const sugeno = new Sugeno(rule_1, rule_2, rule_3)
 *      const result = sugeno.defuzzify(50, 65)
 */
export class Sugeno extends Defuzzifier{
    defuzzify(...values){
        // console.log(values)
        const fraction = this.rules
            .map(rule => {
                // console.log(rule.getThenRuleShape())
                // console.log(rule)
                rule.fuzzValue = rule.fuzzify(...values)
                // console.log(rule)
                return rule
            })
            .map(rule => {
                // console.log(rule.fuzzifier)
                const nominator = rule.fuzzValue * rule.getThenRuleShape().cValue
                const denominator = rule.fuzzValue
                const fraction =  {nominator, denominator}
                // console.log(fraction)
                return fraction
            })
            .reduce((a, b) => {
                const nominator = a.nominator + b.nominator
                const denominator = a.denominator + b.denominator
                return {nominator, denominator}
            })

        const fuzz = fraction.nominator / (fraction.denominator)

        return fuzz ? fuzz : 1
    }
}
