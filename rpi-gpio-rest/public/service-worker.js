const CACHE_NAME = 'carController_v1'
// CODELAB: Add list of files to cache here.
const FILES_TO_CACHE = [
    '/',
    '/index.html',
    '/stylesheets/style.css',
    '/javascripts/index.js',
    '/javascripts/motion-sensors.js',
    '/javascripts/view.js',
    '/javascripts/fuzzy-controller.js',
]

self.addEventListener('install', (evt) => {
    // CODELAB: Precache static resources here.
    evt.waitUntil(
        caches.open(CACHE_NAME).then((cache) => {
            console.log('[ServiceWorker] Pre-caching offline page')
            return cache.addAll(FILES_TO_CACHE)
        })
    )
})

self.addEventListener('fetch', (evt) => {
    evt.respondWith(
        caches.match(evt.request)
            .then(responce => responce || fetch(evt.request))
    )
})


