// This file is a mani js file for the index-old.html file.
// it was used at begining of the development just to test sensor accessibility.

import {
    AbsoluteOrientationSensor,
    RelativeOrientationSensor,
    Gyroscope,
    Accelerometer,
    GravitySensor,
    LinearAccelerationSensor,
} from './motion-sensors.js'

const log_tag = document.getElementById('log')

const sens = (name) => navigator.permissions.query({name})
    .then(function (result) {
        const msg = `\n${name}: ${result.state},`

        const p = document.createElement("p")
        const text = document.createTextNode(msg)
        p.appendChild(text)
        log_tag.appendChild(p)


    })
    .catch(err => {
        const errMsg = `\nfor ${name} err: ${err}`

        const p = document.createElement("p")
        const text = document.createTextNode(errMsg)
        p.className = "red"
        p.appendChild(text)
        log_tag.appendChild(p)
    })

const sensors = [
    'ambient-light-sensor', // chrome blocks it
    'gyroscope',
    'accelerometer',
    'magnetometer',
]

sensors.forEach(s => sens(s))


const options = {frequency: 3, referenceFrame: 'device'}
const sensor_abs = new AbsoluteOrientationSensor(options)
const sensor_rel = new RelativeOrientationSensor(options)
const sensor_gyr = new Gyroscope(options);
const sensor_acs = new Accelerometer(options);
const sensor_grv = new GravitySensor(options);
const sensor_lac = new LinearAccelerationSensor(options);

Promise.all([
    navigator.permissions.query({name: "accelerometer"}),
    navigator.permissions.query({name: "magnetometer"}),
    navigator.permissions.query({name: "gyroscope"})
])
    .then(results => {
        if (results.every(result => result.state === "granted")) {
            // Absolute
            sensor_abs.addEventListener('reading', () => {
                // model is a Three.js object instantiated elsewhere.
                // console.log('Absolute:')
                console.log(sensor_abs)
                // model.quaternion.fromArray(sensor.quaternion).inverse();
                document.getElementById('abs_0').innerText = Math.trunc((sensor_abs.quaternion[0]) * 10000)
                document.getElementById('abs_1').innerText = Math.trunc((sensor_abs.quaternion[1]) * 10000)
                document.getElementById('abs_2').innerText = Math.trunc((sensor_abs.quaternion[2]) * 10000)
                document.getElementById('abs_3').innerText = Math.trunc((sensor_abs.quaternion[3]) * 10000)
            })
            sensor_abs.start()

            // Relative sensor
            sensor_rel.addEventListener('reading', () => {
                // console.log('Relative:')
                console.log(sensor_rel)
                document.getElementById('rel_0').innerText = Math.trunc((sensor_rel.quaternion[0]) * 10000)
                document.getElementById('rel_1').innerText = Math.trunc((sensor_rel.quaternion[1]) * 10000)
                document.getElementById('rel_2').innerText = Math.trunc((sensor_rel.quaternion[2]) * 10000)
                document.getElementById('rel_3').innerText = Math.trunc((sensor_rel.quaternion[3]) * 10000)
            })
            sensor_rel.start()

            // Gyroscope
            sensor_gyr.addEventListener('reading', () => {
                // model is a Three.js object instantiated elsewhere.
                // console.log('Gyroscope:')
                console.log(sensor_gyr)
                // model.quaternion.fromArray(sensor.quaternion).inverse();
                document.getElementById('gyr_x').innerText = Math.trunc((sensor_gyr.x) * 10000)
                document.getElementById('gyr_y').innerText = Math.trunc((sensor_gyr.y) * 10000)
                document.getElementById('gyr_z').innerText = Math.trunc((sensor_gyr.z) * 10000)
                document.getElementById('gyr_t').innerText = sensor_gyr.timestamp
            })
            sensor_gyr.start()

            // Accelerometer
            sensor_acs.addEventListener('reading', () => {
                // model is a Three.js object instantiated elsewhere.
                // console.log('Accelerometer:')
                console.log(sensor_acs)
                // model.quaternion.fromArray(sensor.quaternion).inverse();
                document.getElementById('acs_x').innerText = Math.trunc((sensor_acs.x) * 10000)
                document.getElementById('acs_y').innerText = Math.trunc((sensor_acs.y) * 10000)
                document.getElementById('acs_z').innerText = Math.trunc((sensor_acs.z) * 10000)
            })
            sensor_acs.start()

            // // GravitySensor
            // sensor_grv.addEventListener('reading', () => {
            //     // model is a Three.js object instantiated elsewhere.
            //     // console.log('GravitySensor:')
            //     console.log(sensor_grv)
            //     // model.quaternion.fromArray(sensor.quaternion).inverse();
            //     document.getElementById('grv_x').innerText = Math.trunc((sensor_grv.x) * 10000)
            //     document.getElementById('grv_y').innerText = Math.trunc((sensor_grv.y) * 10000)
            //     document.getElementById('grv_z').innerText = Math.trunc((sensor_grv.z) * 10000)
            // })
            // sensor_grv.start()

            // LinearAccelerationSensor
            sensor_lac.addEventListener('reading', () => {
                // model is a Three.js object instantiated elsewhere.
                console.log(sensor_lac)
                // model.quaternion.fromArray(sensor.quaternion).inverse();
                document.getElementById('lac_x').innerText = Math.trunc((sensor_lac.x) * 10000)
                document.getElementById('lac_y').innerText = Math.trunc((sensor_lac.y) * 10000)
                document.getElementById('lac_z').innerText = Math.trunc((sensor_lac.z) * 10000)
            })
            sensor_lac.start()

        } else {
            console.log("No permissions to use AbsoluteOrientationSensor.")
        }
    })
    .catch(err => console.log({err}))

