"use strict"
// This is Fuzzy controller. It is used to fuzzify values as well
// to map Euler angles to motor values (-100 - 0 - +100)

import { Constant } from '../libraries/my-fuzz/lib/curve/constant.js'
import { Trapezoid } from '../libraries/my-fuzz/lib/curve/trapezoid.js'
import { Triangle } from '../libraries/my-fuzz/lib/curve/triangle.js'
import { Rise} from '../libraries/my-fuzz/lib/curve/rise.js'
import { Fall} from '../libraries/my-fuzz/lib/curve/fall.js'
import { FuzzySet } from '../libraries/my-fuzz/lib/FuzzySet.js'
import { FuzzySetEntry } from '../libraries/my-fuzz/lib/FuzzySetEntry.js'
import { FuzzyRule } from '../libraries/my-fuzz/lib/FuzzyRule.js'
import { Sugeno } from '../libraries/my-fuzz/lib/DefuzzifierSugeno.js'
import { Hedges } from '../libraries/my-fuzz/lib/hedges.js'

// sets
const speed = {
    INSIGNIFICANT: 'insignificant',
    SLOW: 'little',
    AVERAGE: 'average',
    MODERATE: 'moderate',
    FAST: 'fast',
}
const turn = {
    LITTLE: 'little',
    MEDIUM: 'medium',
    LARGE: 'large',
}
const reducer = {
    LITTLE: 'little',
    MEDIUM: 'medium',
    LARGE: 'large',
}
// Fuzzy sets
const speedFuzzySet = new FuzzySet('speed',
    new FuzzySetEntry({entryName: speed.SLOW,     shape: new Fall(20, 40)}),
    new FuzzySetEntry({entryName: speed.MODERATE, shape: new Triangle(25, 50, 65)}),
    new FuzzySetEntry({entryName: speed.FAST,     shape: new Rise(50, 65)}),
)
const turnFuzzySet = new FuzzySet('turn',
    new FuzzySetEntry({entryName: turn.LITTLE, shape: new Fall(10, 35)}),
    new FuzzySetEntry({entryName: turn.MEDIUM, shape: new Triangle(25, 50, 65)}),
    new FuzzySetEntry({entryName: turn.LARGE,  shape: new Rise(50, 65)}),
)
const differentialFuzzySet = new FuzzySet('reducer',
    new FuzzySetEntry({entryName: reducer.LITTLE, shape: new Constant(0.2)}),
    new FuzzySetEntry({entryName: reducer.MEDIUM, shape: new Constant(0.6)}),
    new FuzzySetEntry({entryName: reducer.LITTLE, shape: new Constant(0.8)}),
)
// Fuzzy rules
const fuzzyRules = [
    new FuzzyRule()
        .If('speed', speedFuzzySet.getFuzzyfier(speed.SLOW), Hedges.moreless)
        .And('turn', turnFuzzySet.getFuzzyfier(turn.LITTLE), Hedges.moreless)
        .Then('reducer', differentialFuzzySet.getFuzzyfier(reducer.LITTLE), Hedges.extremely),
    new FuzzyRule()
        .If('speed', speedFuzzySet.getFuzzyfier(speed.MODERATE), Hedges.somewhat)
        .And('turn', turnFuzzySet.getFuzzyfier(turn.MEDIUM))
        .Then('reducer', differentialFuzzySet.getFuzzyfier(reducer.MEDIUM), Hedges.indeed),
    new FuzzyRule()
        .If('speed', speedFuzzySet.getFuzzyfier(speed.FAST), Hedges.extremely)
        .And('turn', turnFuzzySet.getFuzzyfier(turn.LARGE), Hedges.very)
        .Then('reducer', differentialFuzzySet.getFuzzyfier(reducer.MEDIUM), Hedges.somewhat),
    ]


export class FuzzyMotorController {
    constructor() {
    }

    from({pitch, yaw, roll}){
        let turn = Math.abs(roll) -3.2
        turn = Math.abs(turn) * 2000
        const data = {
            direction: Math.trunc(roll),
            speed: Math.trunc(turn)
        }

        const speed = pitch * 2000

        return {
            right: Math.trunc(speed),
            left : Math.trunc(speed),
        }
    }

    ballValuesFrom({pitch, yaw, roll}){
        // pitch: 0 -> (+/-)1.54
        const pitchMax = 1.54
        // roll: (+/-)3.14 -> (+/-)1.54
        const rollMin = 1.54

        const yPos = (y) => (y * 100) / pitchMax
        const xPos = (x) => this.copysign(Math.PI, x) - x
        const inFocus = (n) => Math.abs(n) > 100
            ? this.copysign(100, n)
            : n

        const x = inFocus(yPos(xPos(roll)) * 2)
        let y = inFocus(yPos(-pitch) * 2)

        return {x, y}
    }

    fuzzyBallValues({x, y}){
        // Defuzzifier
        const defuzzifier = new Sugeno(...fuzzyRules)
        const reducer = defuzzifier.defuzzify(Math.abs(x), Math.abs(y))
        const newX = x *(1+reducer)
        const newY = y *(1+reducer)
        const getValue = (n) => Math.abs(n) > 100 ? this.copysign(100, n) : n
        return {x: getValue(newX), y: getValue(newY)}
    }


    /**
     * This method is used to test if fuzzy logic is not broken yet
     * while developing and changing code for it.
     */
    fuzz_test_run(){
        const weaponFuzzySet = new FuzzySet('weapon',
            new FuzzySetEntry({
                entryName: 'harmless',
                shape: new Fall(20, 60)
            }),
            new FuzzySetEntry({
                entryName: 'dangerous',
                shape: new Triangle(20, 50, 80)
            }),
            new FuzzySetEntry({
                entryName: 'lethal',
                shape: new Rise(40, 80)
            }),
        )
        const enemyFuzzySet = new FuzzySet('enemy',
            new FuzzySetEntry({
                entryName: 'weak',
                shape: new Triangle(0, 20, 40)
            }),
            new FuzzySetEntry({
                entryName: 'strong',
                shape: new Triangle(30, 50, 70)
            }),
            new FuzzySetEntry({
                entryName: 'formidable',
                shape: new Triangle(60, 80, 100)
            }),
        )

        const damageFuzzySet = new FuzzySet('damage',
            new FuzzySetEntry({
                entryName: 'small',
                shape: new Constant(20)
            }),
            new FuzzySetEntry({
                entryName: 'partial',
                shape: new Constant(50)
            }),
            new FuzzySetEntry({
                entryName: 'great',
                shape: new Constant(80)
            }),
        )

        console.log(weaponFuzzySet)

        const ruleArr = [
            new FuzzyRule()
                .If('weapon', weaponFuzzySet.getFuzzyfier('lethal'), Hedges.slightly)
                .And('enemy', enemyFuzzySet.getFuzzyfier('formidable'), Hedges.indeed)
                .Then('damage', damageFuzzySet.getFuzzyfier('great')),
            new FuzzyRule()
                .If('weapon', weaponFuzzySet.getFuzzyfier('dangerous'), Hedges.very)
                .And('enemy', enemyFuzzySet.getFuzzyfier('strong'), Hedges.indeed)
                .Then('damage', damageFuzzySet.getFuzzyfier('partial')),
            new FuzzyRule()
                .If('weapon', weaponFuzzySet.getFuzzyfier('harmless'), Hedges.moreless)
                .Or('enemy', enemyFuzzySet.getFuzzyfier('weak'), Hedges.somewhat)
                .Then('damage', damageFuzzySet.getFuzzyfier('small'), Hedges.very, Hedges.very),
        ]

        // console.log(rule_1)
        // console.log(rule_2)
        // console.log(rule_3)

        const sugeno = new Sugeno(...ruleArr)
        const result = sugeno.defuzzify(50, 65)
        console.log(result)
    }

    /**
     * Rewrite of same name C function
     * @param a - value were sighn should be colied
     * @param b - value which sigbn will be used
     * @returns {`a` with the sghn of `b`}
     */
    copysign(a, b) {
        return a < 0 && b > 0 || a > 0 && b < 0
            ? a * -1
            : a
    }
}
