// This is the main js file wich is loaded with `index.html`
import {
    AbsoluteOrientationSensor,
    RelativeOrientationSensor,
} from './motion-sensors.js'

import {FuzzyMotorController} from './fuzzy-controller.js'
import {View} from './view.js'
import {Robot} from './robot.js'

const canvasId = 'viewCanvas'
const url = '/motor'
const fuzzy = new FuzzyMotorController()
const viewCanvas = new View(canvasId)
const robot = new Robot(url)
const options = {frequency: 60, referenceFrame: 'device'}
const sensor_abs = new AbsoluteOrientationSensor(options)
const sensor_rel = new RelativeOrientationSensor(options)

// get permissions for required sensors
Promise.all([
    navigator.permissions.query({name: "accelerometer"}),
    navigator.permissions.query({name: "magnetometer"}),
    navigator.permissions.query({name: "gyroscope"})
])
    .then(results => {
        if (results.every(result => result.state === "granted")) {
            // Absolute
            sensor_abs.addEventListener('reading', () => {
                // console.log(sensor_abs)
                const angles = {}
                const q = {}
                const {quaternion} = sensor_abs

                q.w = (quaternion[0])
                q.x = (quaternion[1])
                q.y = (quaternion[2])
                q.z = (quaternion[3])

                // roll (x-axis rotation)
                const siny_cosp = 2 * (q.w * q.z + q.x * q.y)
                const cosy_cosp = 1 - 2 * (q.y * q.y + q.z * q.z)
                angles.roll = Math.atan2(siny_cosp, cosy_cosp)

                // pitch (y-axis rotation)
                const sinp = 2 * (q.w * q.y - q.z * q.x)
                if (Math.abs(sinp) >= 1) {
                    console.log('if', sinp)
                    angles.pitch = copysign(Math.PI / 2, sinp) // use 90 degrees if out of range
                } else {
                    // console.log('else', sinp)
                    angles.pitch = Math.asin(sinp)
                }

                // yaw (z-axis rotation)
                const sinr_cosp = 2 * (q.w * q.x + q.y * q.z)
                const cosr_cosp = 1 - 2 * (q.x * q.x + q.y * q.y)
                angles.yaw = Math.atan2(sinr_cosp, cosr_cosp)

                // display values in the view
                // const infoPitch = {infoName: 'pitch', value: angles.pitch}
                // const infoRoll = {infoName: 'roll', value: angles.roll}                //
                // viewCanvas.displayInfo(infoPitch, infoRoll)

                // get ball values for the canvas
                let ballPos = fuzzy.ballValuesFrom(angles)
                // fuzzify them
                ballPos = fuzzy.fuzzyBallValues(ballPos)
                // draw ball on the canvas
                viewCanvas.drawBall(ballPos)
                robot.setMotorValues(ballPos)
                robot.drive()
            })
            sensor_abs.start()
        } else {
            console.log("No permissions to use AbsoluteOrientationSensor.")
        }

    })
    .catch(err => console.log({err}))
