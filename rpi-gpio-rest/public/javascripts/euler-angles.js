// This file is test file were we tried conversion from quaternions to Euler angles.
// This file is hosted on `euler.html` page.

import {
    AbsoluteOrientationSensor,
    RelativeOrientationSensor,
} from './motion-sensors.js'

import {
    FuzzyMotorController
} from './fuzzy-controller.js'

const log_tag = document.getElementById('log')

const fuzzy = new FuzzyMotorController()

const options = {frequency: 3, referenceFrame: 'device'}
const sensor_abs = new AbsoluteOrientationSensor(options)
const sensor_rel = new RelativeOrientationSensor(options)


Promise.all([
    navigator.permissions.query({name: "accelerometer"}),
    navigator.permissions.query({name: "magnetometer"}),
    navigator.permissions.query({name: "gyroscope"})
])
    .then(results => {
        if (results.every(result => result.state === "granted")) {
            // Absolute
            sensor_abs.addEventListener('reading', () => {
                // console.log(sensor_abs)
                const angles = {}
                const q = {}
                const {quaternion} = sensor_abs

                q.w = (quaternion[0])
                q.x = (quaternion[1])
                q.y = (quaternion[2])
                q.z = (quaternion[3])

                document.getElementById('abs_0').innerText = q.w
                document.getElementById('abs_1').innerText = q.x
                document.getElementById('abs_2').innerText = q.y
                document.getElementById('abs_3').innerText = q.z

                // Ref: https://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles
                // roll (x-axis rotation)
                const sinr_cosp = 2 * (q.w * q.x + q.y * q.z)
                const cosr_cosp = 1 - 2 * (q.x * q.x + q.y * q.y)
                angles.roll = Math.atan2(sinr_cosp, cosr_cosp)

                // pitch (y-axis rotation)
                const sinp = 2 * (q.w * q.y - q.z * q.x)
                if (Math.abs(sinp) >= 1) {
                    console.log('if', sinp)
                    angles.pitch = copysign(Math.PI / 2, sinp) // use 90 degrees if out of range
                } else {
                    // console.log('else', sinp)
                    angles.pitch = Math.asin(sinp)
                }

                // yaw (z-axis rotation)
                const siny_cosp = 2 * (q.w * q.z + q.x * q.y)
                const cosy_cosp = 1 - 2 * (q.y * q.y + q.z * q.z)
                angles.yaw = Math.atan2(siny_cosp, cosy_cosp)

                document.getElementById('eul_roll').innerText = angles.roll
                document.getElementById('eul_pitch').innerText = angles.pitch
                document.getElementById('eul_yaw').innerText = angles.yaw
            })
            sensor_abs.start()


        } else {
            console.log("No permissions to use AbsoluteOrientationSensor.")
        }

    })
    .catch(err => console.log({err}))

/**
 * Rewrite of same name C function
 * @param a - value were sighn should be colied
 * @param b - value which sigbn will be used
 * @returns {`a` with the sghn of `b`}
 */
function copysign(a, b) {
    return a < 0 && b > 0 || a > 0 && b < 0
        ? a * -1
        : a
}
