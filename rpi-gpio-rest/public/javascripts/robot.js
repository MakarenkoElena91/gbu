// This file is cntains class which contains logic for motor control
// and responsible fr sending data back to server.

// stores las values which was send to server
let lastSentMotorValues = {}

export class Robot {
    constructor(url) {
        this.url = url

        this.motors = {
            left: 0,
            right: 0,
        }

        lastSentMotorValues = {...this.motors}
    }

    setMotorValues({x, y}){
        const xPercent = Math.abs(x / 100)
        const absY = Math.abs(y)
        if(absY < 5){
            this.motors.left = x
            this.motors.right = x * -1
        }else {
            const fraction = (y * xPercent)
            this.motors.left = x < 0 ? y -fraction: y
            this.motors.right = x > 0 ? y -fraction : y
        }
        return this
    }

    drive() {
        // console.log('->>', this.motors, lastSentMotorValues)
        if(this.isChangedEnough(lastSentMotorValues, 0.5)){
            console.log('RUNNNN->', this.motors, lastSentMotorValues)
            fetch(this.url, {
                method: 'POST',
                cache: 'no-cache',
                headers: {
                    'Content-Type': 'application/json'
                    // 'Content-Type': 'application/x-www-form-urlencoded',
                },
                body: JSON.stringify(this.motors),
            })
                // .then(res => console.log(res))
                .catch(err => console.log(err))

            lastSentMotorValues = {...this.motors}
        }
        // console.log('do-nothing', this.motors, lastSentMotorValues)
    }

    isChangedEnough({left, right}, change){
        return (
            Math.abs(left - this.motors.left) > change
            || Math.abs(right - this.motors.right) > change
        )
    }
}
