// This file is responsible for drawing ball on the canvas

export class View {
    constructor(canvasId) {
        this.infoList = []

        const canvas = document.getElementById(canvasId)
        this.ctx = canvas.getContext("2d")
        this.ctx.canvas.width = window.innerWidth   //- 50 * 2
        this.ctx.canvas.height = window.innerHeight //- 50 * 2
        this.ballRadius = 30
        this.X = this.ctx.canvas.width / 2
        this.Y = this.ctx.canvas.height / 2
        this.ballX = this.X
        this.ballY = this.Y

        console.log('View created')
        this.placeBallCanvasPos({})
    }


    drawAxis() {

        this.ctx.beginPath();
        this.ctx.fillStyle='rgba(252,255,214,0.47)';
        this.ctx.rect(0, this.ctx.canvas.height * 3 / 8, this.ctx.canvas.width, this.ctx.canvas.height * 2 / 8)
        this.ctx.fill()
        this.ctx.stroke();


        this.ctx.strokeStyle = '#000000'
        this.ctx.lineWidth = 5
        // vertical y
        this.ctx.beginPath();
        this.ctx.moveTo(this.ctx.canvas.width / 2, 0)
        this.ctx.lineTo(this.ctx.canvas.width / 2, this.ctx.canvas.height)
        this.ctx.stroke()
        // horizontal x
        this.ctx.beginPath()
        this.ctx.moveTo(0, this.ctx.canvas.height / 2)
        this.ctx.lineTo(this.ctx.canvas.width, this.ctx.canvas.height / 2)
        this.ctx.stroke();
    }

    drawExtraLines() {
        this.ctx.strokeStyle = '#000000'
        this.ctx.lineWidth = 1
        this.ctx.beginPath()
        this.ctx.moveTo(this.ctx.canvas.width / 4, 0);
        this.ctx.lineTo(this.ctx.canvas.width / 4, this.ctx.canvas.height)
        this.ctx.stroke();
        this.ctx.beginPath()
        this.ctx.moveTo(this.ctx.canvas.width * 3 / 4, 0);
        this.ctx.lineTo(this.ctx.canvas.width * 3 / 4, this.ctx.canvas.height)
        this.ctx.stroke()
        this.ctx.beginPath()
        this.ctx.moveTo(0, this.ctx.canvas.height / 4)
        this.ctx.lineTo(this.ctx.canvas.width, this.ctx.canvas.height / 4)
        this.ctx.stroke()
        this.ctx.beginPath()
        this.ctx.moveTo(0, this.ctx.canvas.height * 3 / 4)
        this.ctx.lineTo(this.ctx.canvas.width, this.ctx.canvas.height * 3 / 4)
        this.ctx.stroke()


        this.ctx.beginPath()
        this.ctx.moveTo(0, this.ctx.canvas.height * 3 / 8)
        this.ctx.lineTo(this.ctx.canvas.width, this.ctx.canvas.height * 3 / 8)
        this.ctx.stroke()

        this.ctx.beginPath()
        this.ctx.moveTo(0, this.ctx.canvas.height * 5 / 8)
        this.ctx.lineTo(this.ctx.canvas.width, this.ctx.canvas.height * 5 / 8)
        this.ctx.stroke()



    }

    placeBallCanvasPos({x = this.X, y = this.Y}) {
        this.ctx.clearRect(0, 0, this.X * 2, this.Y * 2)
        this.drawAxis()
        this.drawExtraLines()
        this.ctx.beginPath()
        this.ctx.arc(x, y, this.ballRadius, 0, Math.PI * 2)
        this.ctx.fillStyle = "#0095DD"
        this.ctx.fill()
        this.ctx.closePath()

    }

    drawBall({x, y}) {
        // console.log("x & y ", x, y)
        let percentageX = (x / 100)
        let percentageY = (y / 100)

        this.ballX = this.X + this.X * percentageX
        this.ballY = this.Y - this.Y * percentageY

        // console.log("ballX ballY ", this.ballX, this.ballY)

        this.drawAxis()
        this.drawExtraLines()
        this.placeBallCanvasPos({x: this.ballX, y: this.ballY})
    }

    displayInfo(...info) {
        let infos = [...info]
        if (!info) {
            infos = []
        }
        this.ctx.font = "30px Arial"
        const startPos = 50

        // console.log(infos)
        this.placeBallCanvasPos({x: this.ballX, y: this.ballY})

        infos.concat(this.infoList)
            .filter(info => info)
            .forEach((info, index) => {
                const text = `${info.infoName}: ${info.value}`
                this.ctx.fillText(text, 10, startPos + 50 * index)
            })
    }

    attachInfo({infoName, value}) {
        this.infoList.push({infoName, value})
    }

    resetInfoTo({infoName, value}) {
        this.infoList = []
        this.attachInfo({infoName, value})
    }
}
