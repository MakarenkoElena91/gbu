// This is motor control server side file.
//It uses rpio library to control RPI gpio pins.


/*
    Note: To change deployment target on WebStorm:
        - Settings (ctrl + alt + s)
        - Build, Execution, Deployment
        - Deployment
        - '+'
 */

const express = require('express')
const router = express.Router()
const rpio = require('rpio')


// motor green
const     pinPwm_greenMotor = 12    // purple
const pinForward_greenMotor = 16    // blue
const pinReverse_greenMotor = 18    // green
// motor white
const     pinPwm_whiteMotor = 33    // brown
const pinForward_whiteMotor = 22    // red wire
const pinReverse_whiteMotor = 36    // orange

const MAX_SPEED = 2000


const options = {
    gpiomem: false,         /* Use /dev/mem for i²c/PWM/SPI */
    mapping: 'physical',    /* Use the P1-P40 numbering scheme */
    mock: undefined,        /* Emulate specific hardware in mock mode */
}

rpio.init(options)
rpio.open(pinPwm_greenMotor, rpio.PWM)
rpio.open(pinPwm_whiteMotor, rpio.PWM)
const range = 1024
const max = 128
const clockdiv = 8
const interval = 5

rpio.pwmSetClockDivider(clockdiv)
rpio.pwmSetRange(pinPwm_greenMotor, range)
rpio.pwmSetRange(pinPwm_whiteMotor, range)

router.post('/', (req, res, next) => {
    let {right, left} = req.body

    console.log('got: ', Math.trunc(left), Math.trunc(right))
    left = Math.trunc(left *1)
    right = Math.trunc(right *1)
    left = MAX_SPEED * left /100
    right = MAX_SPEED * right /100

    console.log('go:  ',left, right)
    let msg = {}


    // left
    rpio.pwmSetData(pinPwm_whiteMotor, Math.trunc(Math.abs(left)))
    if(left < -1){
        rpio.open(pinForward_whiteMotor, rpio.OUTPUT, rpio.HIGH)
        rpio.open(pinReverse_whiteMotor, rpio.OUTPUT, rpio.LOW)
    }
    else if(left > 1){
        rpio.open(pinForward_whiteMotor, rpio.OUTPUT, rpio.LOW)
        rpio.open(pinReverse_whiteMotor, rpio.OUTPUT, rpio.HIGH)
    }
    else { // left == 0
        rpio.open(pinForward_whiteMotor, rpio.OUTPUT, rpio.LOW)
        rpio.open(pinReverse_whiteMotor, rpio.OUTPUT, rpio.LOW)
    }

    // right
    rpio.pwmSetData(pinPwm_greenMotor, Math.trunc(Math.abs(right)))
    if(right < -1){
        rpio.open(pinForward_greenMotor, rpio.OUTPUT, rpio.HIGH)
        rpio.open(pinReverse_greenMotor, rpio.OUTPUT, rpio.LOW)
    }
    else if(right > 1){
        rpio.open(pinForward_greenMotor, rpio.OUTPUT, rpio.LOW)
        rpio.open(pinReverse_greenMotor, rpio.OUTPUT, rpio.HIGH)
    }
    else { // right == 0
        rpio.open(pinForward_greenMotor, rpio.OUTPUT, rpio.LOW)
        rpio.open(pinReverse_greenMotor, rpio.OUTPUT, rpio.LOW)
    }



    msg.message = `right wheel spinning at: ${right} and left wheel speed: ${left}`
    return res.status(200).json(msg)
})

function stopMotors(){
    rpio.open(pinForward_greenMotor, rpio.OUTPUT, rpio.LOW)
    rpio.open(pinReverse_greenMotor, rpio.OUTPUT, rpio.LOW)
    rpio.pwmSetData(pinPwm_greenMotor, 0)
}

router.post('/drive', (req, res, next) => {
    console.log(req.body)
    const {direction, speed} = req.body
    let msg = {}

    rpio.pwmSetData(pinPwm_greenMotor, speed)


    if (direction < 0) {
        rpio.open(pinForward_greenMotor, rpio.OUTPUT, rpio.LOW)
        rpio.open(pinReverse_greenMotor, rpio.OUTPUT, rpio.HIGH)

        msg.message = 'spinning clockwise'
    } else if (direction > 0) {
        rpio.open(pinForward_greenMotor, rpio.OUTPUT, rpio.HIGH)
        rpio.open(pinReverse_greenMotor, rpio.OUTPUT, rpio.LOW)

        msg.message = 'spinning anti-clockwise'
    } else {
        rpio.open(pinForward_greenMotor, rpio.OUTPUT, rpio.LOW)
        rpio.open(pinReverse_greenMotor, rpio.OUTPUT, rpio.LOW)

        msg.message = 'motor stopped'
    }

    return res.status(200).json(msg)
})

router.delete('/', (req, res, next) => {
    stopMotors()
    return res.status(200).json({message: 'all stopped'})
})


router.post('/', (req, res, next) => {
    console.log(req.body)
    const {ledPin} = req.body

    /*
     * Set the initial state to low.  The state is set prior to the pin
     * being actived, so is safe for devices which require a stable setup.
     */
    rpio.open(ledPin, rpio.OUTPUT, rpio.LOW)

    /*
     * The sleep functions block, but rarely in these simple programs does
     * one care about that.  Use a setInterval()/setTimeout() loop instead
     * if it matters.
     */
    for (let i = 0; i < 5; i++) {
        /* On for 1 second */
        rpio.write(ledPin, rpio.HIGH)
        rpio.sleep(1)

        /* Off for half a second (500ms) */
        rpio.write(ledPin, rpio.LOW)
        rpio.msleep(500)
    }

    return res.status(200).json({message: 'Blinking is done'})
})

module.exports = router
// module.exports = stopMotors
