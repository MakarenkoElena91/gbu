// This is servers side route file for the index.html

const express = require('express')
const router = express.Router()
const child_process = require('child_process');


/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('index', {title: 'Express'})
})

module.exports = router
